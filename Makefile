#!/usr/bin/make -f

all: using_make_for_science.pdf

relevant_xkcd.png:
	wget -O $@ "http://imgs.xkcd.com/comics/automation.png"

%.tex: %.Rnw
	R --encoding=utf-8 -e \
	"library('knitr'); knit('$<')"

%.pdf: %.tex $(wildcard *.bib) $(wildcard *.tex)
	latexmk -pdf \
	-pdflatex='xelatex -shell-escape -8bit -interaction=nonstopmode %O %S' \
	-bibtex -use-make $<
