Using GNU Make for Science
----------------------

A presentation about how I (Don Armstrong) use GNU make to run my
projects and try to do reproducible research.

Examples, pitfalls, and lessons from my history of using make.
